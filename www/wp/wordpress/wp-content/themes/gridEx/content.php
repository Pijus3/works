<?php
        while ( have_posts() ) :
                the_post() ;
        if( is_front_page() ) {
                $classes='full-width';
        } else {
                $classes='half-width';
        }
        ?>
<article <?php echo $classes; ?>>
        <section>
            <h2><?php  the_title();  ?></h2>
            <?php the_content(); ?>
        </section>
</article>

        <?php 
        
                
        endwhile; 
        if( !is_front_page() ) {

                get_sidebar ();  

        }    
?>