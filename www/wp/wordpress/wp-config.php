<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_testas');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '-Nhl`6+At/+Vn31*Qtv-O]~)f/6`2I~1!GQDCsbV(I[p?wpzKup[BG~g=$~%,}L(');
define('SECURE_AUTH_KEY',  'm2mMC)ea +Nz@?6SO!ui OTnh!zxs=A%,>ARG~&=47bAu>:o>0G13-V8 u%wLc0e');
define('LOGGED_IN_KEY',    'O2Ra)SVu5Ea>@F,JVn=mgr`~`*8C&EP6w|Khl^W6JnqPd02JCS0=)~]yg-NUmOCd');
define('NONCE_KEY',        'VE#-26mJ7#r]4Q5I{WVTt<L-Xc0%6G8E0[NBGJ|(W2AX+G|lqw#n3*iYW%]^~#u ');
define('AUTH_SALT',        'uUjY{sp^OWz&1c>rk%2>muVln)D`naUX[(~n^$uO)8L?6M=P=4C( #z8n585- ;q');
define('SECURE_AUTH_SALT', '5%?;aCx<Syd,OncK++LN/d`!:_ei|bcaDl4Qg$bRhuqb;?A?3|8K<Ew piWKt$&-');
define('LOGGED_IN_SALT',   'DL+07T{U(Wc2ryh}KR< fHO7(2sPq[K^[*O7LrrH5Eq2@p~M$y2gm}~/TXG XFQ~');
define('NONCE_SALT',       'h> <eAjdPr4<oS,tN6]$j|5k^RVHx<hM4,|2Mf5-ii_24,4N)~/J&IkF4hYI(G@>');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
