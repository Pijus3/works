var defaultCity = "klaipeda";
var weatherAPI = "http://api.openweathermap.org/data/2.5/weather?q=";
var appID = "28f941a283a599657eacbf65563f7beb"

  //parsiusti duomenis apie Default miesta

  function parsiustiDuomenis(city, weatherAPI, appID) {

    let url = weatherAPI + city + '&units=metric&appid=' + appID;

    httpRequest = new XMLHttpRequest();
    httpRequest.onreadystatechange = uzkrautiDuomenis;
    httpRequest.open('GET', url);
    httpRequest.send();
  }
  parsiustiDuomenis(defaultCity, weatherAPI, appID);

  //duomenis užkrauti į DIV'ą

  function uzkrautiDuomenis() {
    if (httpRequest.readyState === XMLHttpRequest.DONE) {
      if (httpRequest.status === 200) {
        //duomenu apdorojimas jeigu TRUE

        var response = JSON.parse(httpRequest.responseText);

        let oruDivai = document.getElementsByClassName('orai');
        oruDivai[0].innerHTML = response.main.temp + " celcius";

      } else {
          alert("error")

        //duomenu apdorojimas jei FALSE
      }
    }
  }
  document.getElementById('taurage').onmouseover = function () {
      console.log(this.getAttribute('id'));
      parsiustiDuomenis(this.getAttribute('id'), weatherAPI, appID);

  }
  document.getElementById('gargzdai').onmouseover = function () {
    console.log(this.getAttribute('id'));
    parsiustiDuomenis(this.getAttribute('id'), weatherAPI, appID);

}
document.getElementById('klaipeda').onmouseover = function () {
  console.log(this.getAttribute('id'));
  parsiustiDuomenis(this.getAttribute('id'), weatherAPI, appID);

} 
document.getElementById('skuodas').onmouseover = function () {
  console.log(this.getAttribute('id'));
  parsiustiDuomenis(this.getAttribute('id'), weatherAPI, appID);

}
  //kai paspaudžia miestą, parsiųsti duomenis išnaujo ir užkrauti į DIV'ąpaspaudzia miesta, parsiusti duomenis is naujo ir uzkrauti i div

// perdarytas kodas per jqueryy
/*
 $('.hoveris').mouseover (function() {
    $(this).css('background-color', 'grey');
  });
  */

$('.hoveris').mouseenter(function () { 
    $(this).css('background-color', 'grey');
});
$('.hoveris').mouseleave(function () { 
  $(this).css('background-color', 'white');
});