var autos=[
    {data: "2019-08-11 19:59:15", numeris: "HTP-911", nuvaziuota: "5000", trukme: "200"},
    {data: "2019-08-12 19:59:15", numeris: "HTP-911", nuvaziuota: '6000', trukme: '500'},
    {data: "2019-08-13 19:59:15", numeris: 'HTP-911', nuvaziuota: '3000', trukme: '400'},
    {data: "2019-08-14 19:59:15", numeris: 'HTP-911', nuvaziuota: '4000', trukme: '200'},
    {data: "2019-08-15 19:59:15", numeris: 'HTP-911', nuvaziuota: '8000', trukme: '700'},
    {data: "2019-08-16 19:59:15", numeris: 'HTP-911', nuvaziuota: '9000', trukme: '900'},
]
var rodytiGreicius=document.getElementById("greiciai");
var pradiniaiDuomenys=document.getElementById("pradiniaiDuomenys");
var lentele=document.getElementById("lentele");
let greiciuLentele = document.getElementById('greiciuLentele');

// var vidutiniuGreiciuBlokas=document.getElementById("vidutiniaiGreiciai");
// var masyvoPapildymas=document.getElementById("masyvoPapildymas");

rodytiGreicius.addEventListener("click", skaiciuotiGreiti);
pradiniaiDuomenys.addEventListener('click', rodytiPradiniusGreicius);

function rodytiPradiniusGreicius() {
    let lentelesDuomenys=" ";
    for(i=0; i<autos.length; i++) {
        lentelesDuomenys+= "<tr><td>"+autos[i].data+"</td><td>"+autos[i].numeris+"</td><td>"+autos[i].nuvaziuota+"</td><td>"+autos[i].trukme+"</td></tr>"
    }
    lentele.innerHTML=lentelesDuomenys;
}

function skaiciuotiGreiti () {
    let vidutinisGreitis;
    let greiciuLentelesDuomenys=' ';
    var masyvas= [];

    for (i=0; i<autos.length; i++) {
        vidutinisGreitis = (autos[i].nuvaziuota/1000)/(autos[i].trukme/3600);
        masyvas [i] = vidutinisGreitis.toFixed(2);
        greiciuLentelesDuomenys += "<tr><td>"+ masyvas[i] + "</tr></td>"; 

        }

    greiciuLentele.innerHTML=greiciuLentelesDuomenys;
}

/* 

let masyvas = [];//deklaruojamas masyvas

function skaiciuotiGreiti() {
    let vidutinisGreitis;
    for(i=0; i<autos.length; i++) {
        vidutinisGreitis = (autos[i].nuvaziuota/1000)/(autos[i].trukme/3600);
        masyvas[i] = vidutinisGreitis; //į masyvą įrašinėjama
        objektas['greitis'+i] = vidutinisGreitis; // į objektą įrašinėjama su pavadinimu greitis+i, nes negali visi turėti objekto elementai turėti vienodą pavadinimą.
    }
}
*/