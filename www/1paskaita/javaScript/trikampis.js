var sideOne, sideTwo, sideThree, plotas, perimetras, trikampis, koks;
var trikampis=document.getElementById("prTrikampis");
var trikampioPerimetras=document.getElementById("trikampioPerimetras");
var trikampioPlotas=document.getElementById("trikampioPlotas");
// trikampio tikrinimas
document.getElementById("koksTrikampis").addEventListener("click", tikrink);
document.getElementById("perimetras").addEventListener("click", tikrink);
document.getElementById("plotas").addEventListener("click", tikrink);

function tikrink() {
    sideOne=parseInt(document.getElementById("pirmaKrastine").value);
    sideTwo=parseInt(document.getElementById("antraKrastine").value);
    sideThree=parseInt(document.getElementById("treciaKrastine").value);
    if (sideOne==sideTwo && sideOne==sideThree) {
        koks="Lygiakrastis trikampis";
        perimetras=3*sideOne;
        plotas=(Math.pow(sideOne, 2)*(Math.sqrt(3,2)))/4;
    } if(sideOne==sideTwo && sideOne!=sideThree || sideOne!=sideTwo && sideTwo==sideThree || sideOne!=sideTwo && sideOne==sideThree) {
        koks="Lygiasonis trikampis";
    } if(sideOne!=sideTwo && sideOne!=sideThree && sideTwo!=sideThree) {
        koks="Ivairiakrastis trikampis";
    } if(sideOne>(sideTwo+sideThree) || sideTwo>(sideOne+sideThree) || sideThree>(sideOne+sideTwo)) {
        koks="Tokio trikampio nera"
    }  
    trikampis.innerHTML=koks;
    trikampioPerimetras.innerHTML=perimetras;
    trikampioPlotas.innerHTML=plotas;
}

/*
document.getElementById("perimetras").addEventListener("click", skaiciuoktiPerimetra);

function skaiciuoktiPerimetra() {
    sideOne=parseInt(document.getElementById("pirmaKrastine").value);
    sideTwo=parseInt(document.getElementById("antraKrastine").value);
    sideThree=parseInt(document.getElementById("treciaKrastine").value);
    if (sideOne==sideTwo && sideOne==sideThree) {
        perimetras= 3 * sideOne;
    }
    trikampioPerimetras.innerHTML=perimetras;
}
document.getElementById("plotas").addEventListener("click", skaiciuokPlota);
function skaiciuokPlota() {
    sideOne=parseInt(document.getElementById("pirmaKrastine").value);
    sideTwo=parseInt(document.getElementById("antraKrastine").value);
    sideThree=parseInt(document.getElementById("treciaKrastine").value);
    if (sideOne==sideTwo && sideOne==sideThree) {
        plotas=(Math.pow(sideOne, 2)*(Math.sqrt(3,2)))/4; 
    }
    trikampioPlotas.innerHTML=plotas;
}


// lygiakrastis trikampis perimetras  P=3a , plotas = s= (a^2 * sqrt3) / 4 ; 
*/