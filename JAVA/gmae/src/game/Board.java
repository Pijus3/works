package game;

import java.util.*;
import java.time.*;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Scanner;

import java.io.LineNumberReader;
import java.io.FileReader;
import java.io.IOException;

public class Board extends JPanel implements ActionListener {

    private final int B_WIDTH = 600;
    private final int B_HEIGHT = 600;
    private final int DOT_SIZE = 10;
    private final int ALL_DOTS = 1800;
    private final int RAND_POS = 55;
    private int DELAY=140;
    private int enemyDELAY = 140;

    private final int x[] = new int[ALL_DOTS];
    private final int y[] = new int[ALL_DOTS];

    private final int x1[] = new int[ALL_DOTS];
    private final int y1[] = new int[ALL_DOTS];
    
    private int z = 0;
    

    private String msg = "";  
    private Font fontSize;   
    private Font metrSize; 

    private String remaining = "10";
    private String score = "0";
    private int dots;
    private int enemyDots;
    private int apple_x;
    private int apple_y;

    private boolean leftDirection = false;
    private boolean rightDirection = true;
    private boolean upDirection = false;
    private boolean downDirection = false;
    private boolean inGame = true;

    private boolean enemyLeft = false;
    private boolean enemyRigth = true;
    private boolean enemyUp = false;
    private boolean enemyDown = false;

    private Timer timer;
    private Image ball;
    private Image apple;
    private Image head;

    public Board() {
        
        initBoard();
    }
    
    private void initBoard() {

        addKeyListener(new TAdapter());
        setBackground(Color.black);
        setFocusable(true);

        setPreferredSize(new Dimension(B_WIDTH, B_HEIGHT));
        loadImages();
        initGame();
    }

    private void loadImages() {

        ImageIcon iid = new ImageIcon("src/gmae/dot.png");
        ball = iid.getImage();

        ImageIcon iia = new ImageIcon("src/gmae/dot.png");
        apple = iia.getImage();

        ImageIcon iih = new ImageIcon("src/gmae/head.png");
        head = iih.getImage();
    }

    private void initGame() {
        enemyDots = 4;
        dots = 4;

        for (int z = 0; z < dots; z++) {
            x[z] = 50 - z * 10;
            y[z] = 50;
        }
        for ( int i=0; i < enemyDots; i++ ) {
            x1[i] = 50 - i * 10;
            y1[i] = 50;
        }
        timer = new Timer(DELAY, this);
        timer.start();
        locateApple();
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        doDrawing(g);
    }
    
    private void doDrawing(Graphics g) {
        
        if (inGame) {

            g.drawImage(apple, apple_x, apple_y, this);
            for (int z = 0; z < dots; z++) {
                if (z == 0) {
                    g.drawImage(head, x[z], y[z], this);
                } else {
                    g.drawImage(ball, x[z], y[z], this);
                }
                gameScore(g);
            }
            for ( int i=0; i < enemyDots; i++) {
                if ( i== 0 ) {
                    g.drawImage(head, x1[i], y1[i], this);
                } else {
                    g.drawImage(ball, x1[i], y1[i], this);
                }
            }

            Toolkit.getDefaultToolkit().sync();
        }
        else {
            gameOver(g);
        }        
    }
    private void victoryGame() {
            inGame = false;            
    }

    private void gameOver(Graphics g) {
        Font small = new Font("Helvetica", Font.BOLD, 24);
        Font large = new Font("Helvetica", Font.BOLD, 68);
        if ( z == 10 ) {
            msg = "Victory";
            fontSize = large;
            metrSize = large;
            g.setColor(Color.GREEN);
/* 1.Load current userName from file,( then save userName + score into array and leaderboard)
		String currentName = "";
        Scanner sc = new Scanner(new File("currentUser.txt"));
        currentName=sc.nextString();
		System.out.println("Ivesta: "+currentName);
*/
        }
        else {
            msg = "Game Over";
            fontSize = small;
            metrSize = small;
            g.setColor(Color.RED);
            
        }
        FontMetrics metr = getFontMetrics(metrSize);
        g.setFont(fontSize);
        g.drawString(msg, (B_WIDTH - metr.stringWidth(msg)) / 2, B_HEIGHT / 2);
        g.drawString(score, (B_WIDTH - metr.stringWidth(score)) / 2 , B_HEIGHT /3 );
    } 
    private void gameScore ( Graphics g) {
    	
    	Font small = new Font("Helvetica", Font.BOLD, 14);
		FontMetrics metr = getFontMetrics(small);
        g.setColor(Color.white);
        g.setFont(small);
        g.drawString("Score " + score, (B_WIDTH - metr.stringWidth(score)) - 80 , B_HEIGHT - 580 );
        g.drawString("Remaining " + remaining, (B_WIDTH - metr.stringWidth(score)) - 90 , B_HEIGHT - 560 );

    }

    private void checkApple() {

        if ((x[0] == apple_x) && (y[0] == apple_y)) {
            dots++; 
            Speed();
            Score();
        }
        if ( (x1[0] == apple_x) && (y1[0] == apple_y )) {
            enemyDots++;
            enemyScore();
        }
    }
    private void Speed () {
        this.DELAY -= 10; 
        timer = new Timer(DELAY, this);
        timer.start();
    }
    private void Score () {
        this.z++;
        score = Integer.toString(z);
        remaining = Integer.toString(10-z);
        locateApple();
        if ( z== 10 ) {
            victoryGame();
        }
    }
    private void enemyScore() {
        locateApple();
        if ( z == 10) {
            victoryGame();
        }
    }
    private void move() {

        for (int z = dots; z > 0; z--) {
            x[z] = x[(z - 1)];
            y[z] = y[(z - 1)];
        }
        if (leftDirection) {
            x[0] -= DOT_SIZE;
        }
        if (rightDirection) {
            x[0] += DOT_SIZE;
        }
        if (upDirection) {
            y[0] -= DOT_SIZE;
        }
        if (downDirection) {
            y[0] += DOT_SIZE;
        }
    }
    private void moveEnemy() {

        for (int z = dots; z > 0; z--) {
            x1[z] = x1[(z - 1)];
            y1[z] = y1[(z - 1)];
        }/*
        if ( apple_x < x1[0] ) {
            enemyLeft = true;
            x1[0] -= DOT_SIZE;
        }
        if ( apple_x > x1[0]  ) {
            enemyRigth = true;
            x1[0] += DOT_SIZE;
        }
        if ( apple_y < y1[0] ) {
            enemyUp = true;
            y1[0] -= DOT_SIZE;
        }
        if ( apple_y > y1[0] ) {
            enemyDown = true;
            y1[0] += DOT_SIZE;
        }*/

/*
        final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
        executorService.scheduleAtFixedRate(App::myTask, 0, 1, TimeUnit.SECONDS);
        
        private static void myTask() {
            System.out.println("Running");
        }
*/
        Random rand = new Random();
        int n = rand.nextInt(4);
        switch (n) {
            case 0 : enemyDown = true;
            y1[0] += DOT_SIZE;
            break;
            case 1 : enemyRigth = true;
            x1[0] += DOT_SIZE;
            break;
            case 2 : enemyUp = true;
            y1[0] -= DOT_SIZE;
            break;
            case 3 : enemyLeft = true;
            x1[0] -= DOT_SIZE;
            break;
            default: enemyUp = true;
            y1[0] -= DOT_SIZE;
            break;                                                                            
        }
    }
    // snake collision and enemy collision
    private void checkCollision() {

        for (int z = dots; z > 0; z--) {

            if ((z > 4) && (x[0] == x[z]) && (y[0] == y[z])) {
                inGame = false;
            }
        }
        if (y[0] >= B_HEIGHT) {
            inGame = false;
        }

        if (y[0] < 0) {
            inGame = false;
        }

        if (x[0] >= B_WIDTH) {
            inGame = false;
        }

        if (x[0] < 0) {
            inGame = false;
        }
        
        if (!inGame) {
            timer.stop();
        }
    }

    private void locateApple() {

        int r = (int) (Math.random() * RAND_POS);
        apple_x = ((r * DOT_SIZE));

        r = (int) (Math.random() * RAND_POS);
        apple_y = ((r * DOT_SIZE));

    }

    @Override
    public void actionPerformed(ActionEvent e) {
    	System.out.println("action performed");
        if (inGame) {
            checkApple();
            checkCollision();
            moveEnemy();
            move();
            System.out.println("action performed - - move");
        }
        repaint();
    }

    private class TAdapter extends KeyAdapter {

        @Override
        public void keyPressed(KeyEvent e) {

            int key = e.getKeyCode();

            if ((key == KeyEvent.VK_LEFT) && (!rightDirection)) {
                leftDirection = true;
                upDirection = false;
                downDirection = false;
            }

            if ((key == KeyEvent.VK_RIGHT) && (!leftDirection)) {
                rightDirection = true;
                upDirection = false;
                downDirection = false;
            }

            if ((key == KeyEvent.VK_UP) && (!downDirection)) {
                upDirection = true;
                rightDirection = false;
                leftDirection = false;
            }

            if ((key == KeyEvent.VK_DOWN) && (!upDirection)) {
                downDirection = true;
                rightDirection = false;
                leftDirection = false;
            }
        }
    }
    
}
