package game;

import javax.swing.JFrame;
import java.io.FileNotFoundException;

public class Snake extends JFrame {

    public Snake() {
       initUI();
    }

    private void initUI() {
        
        add(new Board());
        
        setResizable(false);
        pack();
        
        setTitle("Snake");
        setLocationRelativeTo(null);
        
    }

    public static void main(String[] args) throws FileNotFoundException {

            //paleidzia 
            JFrame ex = new Intro();
            ex.setVisible(false);

    }
}