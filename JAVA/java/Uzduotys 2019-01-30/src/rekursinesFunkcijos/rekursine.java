package rekursinesFunkcijos;

public class rekursine {
	
	static int faktorialas (int sk) {
		if (sk == 1 ) return 1;
		return sk*faktorialas(sk-1);
	}
	
	public static void main(String[] args) {
		
		System.out.println(faktorialas(4));

	}

}