package game;

import java.awt.EventQueue;
import javax.swing.JFrame;
import java.awt.*;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Scanner;

import java.io.LineNumberReader;
import java.io.FileReader;
import java.io.IOException;


public class Intro extends Snake {

    protected String userName = "";
   
    public String getUserName () {
        return this.userName;
    }
    public String setUserName ( String name ) {
        return this.userName = name;
    }

    public Intro() {
// frame creation
        JFrame window = new JFrame ("Snake Game v0.21");
        window.getContentPane().setBackground(Color.BLACK);
        window.setSize(300, 300);

// panel creation        
        JPanel introScreen = new JPanel();
        introScreen.setBackground(Color.BLACK);
        window.add(introScreen);
        setResizable(false);
        pack();
        setLocationRelativeTo(null);
// button creation
        JButton newGame = new JButton("New Game");
// name input creation
		JTextField nameInput = new JTextField(20);
        nameInput.setText("Mano Vardas");

// intro screen pridedami mygtukai / inputai
        introScreen.add(newGame);
        introScreen.add(nameInput);


        window.setSize(600,600);
        window.setVisible(true);
        
        newGame.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                // run snake.java
                EventQueue.invokeLater(() -> {
                    JFrame ex = new Snake();
                    ex.setVisible(true);
                    /*
                    // Save userName to file currentUser.txt from nameInput
                    Intro currentPlayer = new Intro(setUserName(nameInput.getText()));

                    Intro.setUserName(currentPlayer());
                    //pw.print(nameInput.getText());
                    //pw.close();
                    System.out.println("Current player name is: " );
                    */
                });		
            }
        });
        
    }
    public static void main(String[] args) throws FileNotFoundException {

        EventQueue.invokeLater(() -> {
            //paleidzia 
            JFrame ex = new Intro();
            ex.setVisible(true);
        });

        //File file = new File("currentUser.txt");
        //PrintWriter pw=new PrintWriter(file);
        //pw.print(nameInput.getText());
        //pw.close();

    }


}