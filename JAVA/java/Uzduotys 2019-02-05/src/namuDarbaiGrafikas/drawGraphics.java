package namuDarbaiGrafikas;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JComponent;
import javax.swing.JPanel;

public class drawGraphics extends JPanel {
	
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawLine(0,300,600,300);
		g.drawLine(300,0,300,600);
		g.drawString("0", 290, 290);
		g.setColor(Color.BLACK);

		for(int x=-300; x<300; x++) {
			int y=getY(x);
			g.drawOval(300+x, 300-y, 1, 1);
		}
	}
	protected int getY(int x) {
		return x;
	}
	
}
