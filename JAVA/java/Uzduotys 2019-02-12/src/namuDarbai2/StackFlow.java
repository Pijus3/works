package namuDarbai2;

public class StackFlow<T> {
	Stack<T> k = null;
	
	public void add( int index, T data ) {
		Stack<T> tmp = k;
		if ( index == 0) {
			push(data);
			return;
		}
		for ( int i=0; i<index-1; i++) {
			tmp= tmp.next;
		}
		Stack<T> laikinas = new Stack<>(data);
		laikinas.next= tmp.next;
		tmp.next = laikinas;
	}
	
	public void remove( int index ) {
		Stack<T> tmp = k;
		for ( int i=0; i < index-1; i++) {
			tmp=tmp.next;
		}
		tmp.next = tmp.next.next;
	}	
	
	public void push( T data ) {
		if (k ==null) {
			k = new Stack<T>(data);
		} else {
			Stack<T> tmp = new Stack<T>(data);
		tmp.next=this.k;
		this.k = tmp;
			}
	}
	
	public T pop() {
		if (k==null) {
			return null;
		} else {
			T tmp = k.data;
			k = k.next;
			return tmp;
		}
	}
	
	
}
