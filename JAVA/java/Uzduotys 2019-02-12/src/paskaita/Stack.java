package paskaita;

public class Stack<T> {
	
	sarasas<T> q = null;
	
	public void push( T data ) {
		if (q==null) {
			q = new sarasas<T>(data);
		} else {
		sarasas<T> tmp = new sarasas<T>(data);
		tmp.next=this.q;
		this.q = tmp;
			}
	}
	
	public T pop() {
		if (q==null) {
			return null;
		} else {
			T tmp = q.data;
			q = q.next;
			return tmp;
		}
	}
	
	public T get(int index) {
		sarasas<T> tmp = q;
		for ( int i=0; i<=index-1; i++) {
			tmp = tmp.next;
		}
		return tmp.data;
	}
	
}
