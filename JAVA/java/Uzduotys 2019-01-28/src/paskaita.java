import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class paskaita {

	public static void main(String[] args) throws FileNotFoundException {
		Scanner in = new Scanner (new File ("paskaita.txt"));
		int darbuotoju = in.nextInt();
		int pardavimu = in.nextInt();
		int [] masyvas = new int [darbuotoju];
		
		for (int i=0; i<pardavimu; i++) {
			int kurisDarbuotojas = in.nextInt();
			int kiekPardave = in.nextInt();
			masyvas[kurisDarbuotojas-1] += kiekPardave;
		}
		
		for (int i=0; i<darbuotoju; i++) {
			System.out.print(masyvas[i] + " ");
		}
		in.close();
	}

}
