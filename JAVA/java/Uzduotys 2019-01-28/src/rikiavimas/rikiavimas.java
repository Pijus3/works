package rikiavimas;

import java.io.PrintWriter;
import java.util.Arrays;

public class rikiavimas {

	public static void main(String[] args) {
		
		int[] masyvas = { 5, 9, 4, 8, 2, 7, 3, 1, 6 };
		
		Arrays.sort(masyvas);
		
		System.out.print("Maziausios reiksmes masyvo " +masyvas[0]+ " "+ masyvas[1]);
		System.out.println();
		System.out.print("Didziausios reiksmes masyvo " +masyvas[masyvas.length-1]+ " "+ masyvas[masyvas.length-2]);
		System.out.println();
		
		/*
		for (int i=0; i<masyvas.length-1; i++) {
			for (int y=i+1; y<masyvas.length; y++) {
				if (masyvas[i]>masyvas[y]) {
					int keiciamas = masyvas[i];
					masyvas[i] = masyvas[y];
					masyvas[y] = keiciamas;
				}
			}
		}
		*/
		for (int i=0; i<masyvas.length; i++) {
			System.out.print(masyvas[i] + " ");
		}
		/*
		 * Ivedimas i sukurta tekstini faila
		 * 
		FileWriter fw = new FileWriter("isvedimas.txt");
		PrintWriter pw = new PrintWriter("isvedimas.txt");
		pw.print("labas");
		pw.close();
		*/
		
	}
}
