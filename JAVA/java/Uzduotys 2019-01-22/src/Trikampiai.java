import java.util.Scanner;

public class Trikampiai {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner in = new Scanner (System.in);
		
		System.out.print("Iveskite pirma krastine : " );
		int a = in.nextInt();
		System.out.print("Iveskite antra krastine : " );
		int b = in.nextInt();
		System.out.print("Iveskite trecia krastine : " );
		int c = in.nextInt();
		
		in.close();
		
		if ( a+b>c && a+c>b && c+b>a ) {
			System.out.println("Imanoma sudaryti trikampi");
		} else {
			System.out.println("Nera tokio trikampio");
		}
		
	}

}
