import java.util.Scanner;

public class Plyteles {

	public static void main(String[] args) {
		var scanner = new Scanner(System.in);
		
		System.out.print("Iveskite patalpos ploti");
		int plotis = scanner.nextInt();
		
		System.out.print("Iveskite patalpos ilgi");
		int ilgis = scanner.nextInt();
		
		System.out.print("Iveskite vieno kvadrato kaina");
		double kaina = scanner.nextDouble();
		
		System.out.print("Iveskite numanoma nuostoliu kieku procentais");
		double nuostoliai = scanner.nextDouble();
		
		scanner.close();
		
		int plotas = plotis * ilgis;
		
		double pirktiPlyteliu = plotas * kaina;
		double papildomiNuostoliai = pirktiPlyteliu * nuostoliai;
		
		System.out.println(plotas + " plotui sumokesite " + pirktiPlyteliu);	
		System.out.println("Papildomiem nuostoliams teks pirkti plyteliu uz: " +(pirktiPlyteliu + papildomiNuostoliai));

	}

}
