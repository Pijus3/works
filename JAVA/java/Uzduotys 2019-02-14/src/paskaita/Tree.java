package paskaita;

public class Tree {
	
	public Item root=null;
	
	public Item add(Item r, int data) {
		if (r==null) return new Item(data);
		if (r.data>data) r.left=add(r.left, data);
		else r.right=add(r.right, data);
		return r;
	}
	
	public void add(int data) {
		if (root==null) {
			root = new Item (data);
			return;
		}
		
		if (root.data > data) {
			root.left = add(root.left, data);
		} else {
			root.right=add(root.right, data);
		}
		
	}
	
	public void print(Item r) {
		if (r!=null) {
		print(r.right);
		System.out.println("Desine puse data " + r.right);
		System.out.println(r.data);
		System.out.println("Kaire puse data " + r.left);
		print(r.left);
		}
	}
	
	public void remove( Item r, int i ) {
		if(r.data>i) {
			System.out.println("r.data > i");
			if(r.left!=null) {
				System.out.println("r.left != null");
				if(r.left.data==i) {
					System.out.println("r.left.data == i");
					r.left = null;
				} else {
					remove(r.left, i);
				}
			}
		} else if (r.data < i) {
			System.out.println("r.data < i");
				if(r.right!=null) {
					System.out.println("r.right != null ");
					if(r.right.data==i) {
						System.out.println("r.right.data == i");
						r.right = null;
					} else {
						remove(r.right, i);
					}
				}
		}
	}
	
}
