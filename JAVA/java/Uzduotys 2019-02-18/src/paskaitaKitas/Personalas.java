package paskaitaKitas;

import java.util.LinkedList;
import java.util.List;

public class Personalas {
	public List<Darbuotojas> personalas = new LinkedList<>();
	public List<NaujasDarbuotojas> naujasDarbuotojas=new LinkedList<>();
	
	public void addDarbuotojas(Darbuotojas d) {
		personalas.add(d);
		for (NaujasDarbuotojas nd:naujasDarbuotojas) {
			nd.onAdd(d);
		}
	}
	
	public void setNaujasDArbuotojasListener(NaujasDarbuotojas  naujasDarbuotojas) {
		 this.naujasDarbuotojas.add(naujasDarbuotojas);
	}
	
}
