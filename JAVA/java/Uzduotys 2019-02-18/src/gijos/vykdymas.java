package gijos;

import java.awt.event.ActionEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class vykdymas {

	public static void main(String[] args) {
		
		JFrame frame = new JFrame("Zaidimas atspek");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		
		JPanel panele = new JPanel();
		panele.setLayout(null);
		
		JButton but = new JButton("Pateikti rezultata");
		but.setBounds(225, 150, 150, 30);
		
		JLabel label = new JLabel();
		JLabel label2 = new JLabel();
		JLabel label3 = new JLabel();
		JLabel label4 = new JLabel();
		
		label.setBounds(265, 180, 100, 30);
		label2.setBounds(275, 50, 100, 30);
		label3.setBounds(275, 250, 100, 30);
		label4.setBounds(100, 100, 100, 30);
		
		JTextField laukelis = new JTextField();
		
		laukelis.setBounds(225, 100, 150, 30);
		panele.add(but);
		
		panele.add(laukelis);
		
		panele.add(label);
		panele.add(label2);
		panele.add(label3);
		panele.add(label4);
		
		Skaiciavimas sk = new Skaiciavimas();
		sk.isvedimas = label;
		sk.uzdavinys = label2;
		sk.uzdavinys();
		sk.start();
		
		but.addActionListener( (ActionEvent e) -> {
			int score = 0;
			String textFieldValue = laukelis.getText();
			if ( sk.tikrinimas() == Integer.parseInt(textFieldValue) )  {
				label3.setText("Teisingas");
				score++;
			}
			else {
				label3.setText("Neteisingai");
			}
			label4.setText("Taskai : "+Integer.toString(score));
		});

		
		frame.add(panele);
		frame.setSize(600, 600);
		frame.setVisible(true);
		
	}

}
