package gijos;

import java.util.Random;

import javax.swing.JLabel;

public class Skaiciavimas extends Thread {
	
	public JLabel isvedimas;
	public JLabel uzdavinys;
	public JLabel atsakymas;
	public int rezultatas = 0;
	Random random = new Random();
	
	public void run() {
		for (int i=5; i>=0; i--) {
			try {
				sleep(1000);
			} catch (Exception e) {
				
			}
			if ( i == 0 ) {
				isvedimas.setText("Laikas baigesi....");
				uzdavinys();
				run();
				try {
					sleep(1000);
				} catch (Exception e) {
					
				}
				
			}
			isvedimas.setText("Jums liko: "+i);
		}
		atsakymas.setText("Game over");
		
	}
	
	public void uzdavinys() {
		int n = random.nextInt(10);
		int m = random.nextInt(10);
		int z = random.nextInt(3) + 1;
		String uzd = null;
		
		switch (z) {
		case 1: 
			this.rezultatas = n*m;
			uzd = "*";
			break;
		case 2: 
			this.rezultatas = n+m;
			uzd = "+";
			break;
		case 3:
			this.rezultatas = n-m;
			uzd = "-";
			break;
		}
		uzdavinys.setText(n + uzd + m);
	}
	
	public int tikrinimas() {
		return this.rezultatas;
	}
}
