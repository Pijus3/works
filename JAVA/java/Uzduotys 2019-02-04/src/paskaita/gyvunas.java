package paskaita;

public class gyvunas {
	public String name;
	public int age;
	public int registracijosNumeris;
	
	
	
	public gyvunas(String name, int age) {
		super();
		this.name = name;
		this.age = age;
	}

	public  void isvestiInfo ( ) {
		System.out.println("Gyvunas : " + this.name + " amzius : " +this.age);
		System.out.println("Registracijos nr : " +getRegNr());
	}
	
	public void setRegistracijosNumeris ( int x ) {
		this.registracijosNumeris = x;
	}
	
	public final String getRegNr () {      // final, sios reiksmes ar metodo nebegalima bus keisti vaiku klasese
		return "REG-" + this.registracijosNumeris;
	}
	
}
