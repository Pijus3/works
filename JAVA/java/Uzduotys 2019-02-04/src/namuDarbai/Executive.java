package namuDarbai;

public class Executive extends Employee {
	
	protected double bonus=0;
	
	public Executive ( String name, String surname, int phone, int socDraudimoNr, double menAtlygis, double bonus ) {
		super (name, surname, phone, socDraudimoNr, menAtlygis);
		this.bonus = bonus;
	}
	
	public double awardBonus ( double x ) {
		return this.bonus = this.bonus + x;
		
	}
	public double pay() {
		return menAtlygis + bonus;
	}
	
	
}
