package namuDarbai;

public class Hourly extends Employee {
	
	protected int hoursWorked = 0;
	protected double hoursPay = 2.45;
	
	public Hourly (String name, String surname, int phone, int socDraudimoNr, double menAtlygis, int hoursWorked) {
		super ( name, surname, phone, socDraudimoNr, menAtlygis);
		this.hoursWorked = hoursWorked;
	}
	
	public double pay() {
		return this.hoursPay * this.hoursWorked;
	}
	
}
