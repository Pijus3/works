package namuDarbai;

public class vykdymas {

	public static void main(String[] args) {
		
		Staff firma = new Staff();
		StaffMember pirmas = new Trainee("Jozas", "Jozas", 36899999);
		StaffMember antras = new Hourly("Petras", "Petras", 36481584, 24, 0, 8 );
		StaffMember trecias = new Employee("Albinas", "Albinas", 3688568, 25, 450 );
		StaffMember ketvirtas = new Employee("Kestas", "Kestas", 36899754, 26, 550);
		StaffMember penktas = new Employee("Dziugas", "Dziugas", 36899999, 27, 600);
		StaffMember sestas = new Executive("Cicinas", "Cicinas", 36899999, 28, 800, 50);
				
		firma.addStaffMember(pirmas);
		firma.addStaffMember(antras);
		firma.addStaffMember(trecias);
		firma.addStaffMember(ketvirtas);
		firma.addStaffMember(penktas);
		firma.addStaffMember(sestas);
		
		((Executive) firma.darbuotojai[5]).awardBonus(100);
		
		System.out.println(firma.payDay());
		
	}

}
