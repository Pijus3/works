package namuDarbai;

public class Employee extends StaffMember {
	protected int socDraudimoNr;
	protected double menAtlygis=0;
	
	public Employee ( String name, String surname, int phone, int socDraudimoNr, double menAtlygis ) {
		super(name, surname, phone);
		this.socDraudimoNr = socDraudimoNr;
		this.menAtlygis = menAtlygis;
	}
	
	//metodas atlyginimo 
	public double pay() {
		return this.menAtlygis;
	}
	
	
	
}
