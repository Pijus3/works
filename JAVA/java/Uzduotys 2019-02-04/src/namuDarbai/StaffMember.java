package namuDarbai;

public class StaffMember extends Staff {
	
	protected String name;
	protected String surname;
	protected int phone;
	
	// konstuktorius
	public StaffMember ( String name, String surname, int phone ) {
		this.name = name;
		this.surname = surname;
		this.phone = phone;
	}
	// seteriai geteriai 
	public void setName ( String name ) {
		 this.name = name;
	}
	public String getName () {
		return this.name;
	}
	public void setSurname ( String surname ) {
		this.surname = surname;
	}
	public String getSurname () {
		return this.surname;
	}
	public void setPhone( int phone ) {
		this.phone = phone;
	}
	public int getPhone () {
		return this.phone;
	}
	// metodai 
	public String toString () {
		return "Vardas : " + this.name + " Pavarde : "+ this.surname + " Numeris tel : " + this. phone;
	}
	public double pay() {
		return 0;
	}
	
}
