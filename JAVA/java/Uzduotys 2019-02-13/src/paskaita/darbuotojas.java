package paskaita;

import java.io.File;
import java.util.Scanner;

public class darbuotojas {
	private String name;
	private int salary;
	
	public darbuotojas(String name, int salary) {
		super();
		this.name = name;
		this.salary = salary;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}
	
	public static darbuotojas load(String fileName) {
		try {
			Scanner in = new Scanner(new File(fileName));
			String name = in.next();
			int salary = in.nextInt();
			darbuotojas n = new darbuotojas(name, salary);
			in.close();
			return n;
		} catch ( Exception e ) {
			return null;
		}
		
	}
	
	public static void printInfo () {
		System.out.println();
	}

}
