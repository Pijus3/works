package namuDarbai;

public class line {
	
	private kordinates x1;
	private kordinates x2;
	
	//konstruktorius
	public line ( kordinates x1, kordinates x2) {
		this.x1 = x1;
		this.x2 = x2;
	}
	
	//metodai
	public double atstumas() {
		return kordinates.round(Math.sqrt((Math.pow((x2.getX()-x1.getX()), 2)+Math.pow((x2.getY()-x1.getY()), 2))),2);
	}
	public String toString() {
		return "["+this.x1+":"+this.x2+"]";
	}
	
}
