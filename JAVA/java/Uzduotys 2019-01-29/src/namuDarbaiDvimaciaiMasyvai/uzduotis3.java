package namuDarbaiDvimaciaiMasyvai;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class uzduotis3 {
 
	public static void main(String[] args) throws FileNotFoundException {
		Scanner in = new Scanner (new File ("uzduotis3.txt"));
		
		int n = in.nextInt();
		int m = in.nextInt();
		
		System.out.println("Skaicius n : " + n);
		System.out.println("Skaicius m : " + m);
		
		int[][] masyvas = new int[n][m];
		
		for ( int i=0; i<masyvas.length; i++ ) {
			for ( int y=0; y<masyvas[i].length; y++ ) {
				masyvas[i][y] = in.nextInt();
			}
		}
		
		for ( int[] row : masyvas ) {
			for ( int col : row ) {
				System.out.print(col + "\t");
			}
			System.out.println();
		}
		in.close();
		
		System.out.println();
		
		in = new Scanner (new File ("uzduotis3.txt"));
		
		int x = in.nextInt();
		int y = in.nextInt();
		
		int[][] pasukimas = new int[y][x];
		
		for (int i=0; i<pasukimas.length; i++) {
			for (int z=0; z<pasukimas[i].length; z++) {
				pasukimas[i][z] = in.nextInt();
			}
		}
		for ( int[] row : pasukimas ) {
			for ( int col : row ) {
				System.out.print(col + "\t");
			}
			System.out.println();
		}

		

		in.close();
	}

}