package namuDarbaiDvimaciaiMasyvai;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class uzduotis1 {

	public static void main(String[] args) throws FileNotFoundException {
		Scanner in = new Scanner (new File ("uzduotis1.txt"));
		
		int n = in.nextInt();
		
		int[][] temperaturos = new int[n][7];
		
		for ( int i=0; i<temperaturos.length; i++ ) {
			for ( int y=0; y<temperaturos[i].length; y++ ) {
				temperaturos[i][y] = in.nextInt();
			}
		}

		int savaite=0;
		
		for ( int[] sk:temperaturos ) {
			int vidurkis = 0;
			savaite++;
			for ( int temp:sk ) {
				vidurkis += temp;
			}
			System.out.println(savaite + " savaites vidurkis : " + vidurkis);
		}
		
		
		for ( int i=0; i<temperaturos.length; i++ ) {
			for ( int y=0; y<temperaturos[i].length; y++ ) {
				System.out.print(temperaturos[i][y] + "\t");
			}
			System.out.println();
		}
		
		in.close();
	}

}
