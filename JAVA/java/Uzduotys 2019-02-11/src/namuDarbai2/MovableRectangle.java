package namuDarbai2;

public class MovableRectangle extends MovablePoint implements Movable, Resizible {
	
	protected int width;
	protected int heigth;
		
	public MovableRectangle(int x, int y, int SpeedX, int SpeedY, int width, int heigth) {
		super(x, y, SpeedX, SpeedY);
		this.width = width;
		this.heigth = heigth;
	}
	public int resizible (int prec)  {
		this.width = this.width + (this.width * prec)/100;
		this.heigth = this.heigth + (this.heigth *prec)/100;
		return this.width;
	}	

}
