package namuDarbai2;

import java.util.*;

public class vykdymas {

	public static void desinen(List<MovablePoint> sarasas) {
		for ( MovablePoint g:sarasas) {
			g.moveRight();
		}
	}
	public static void kairen(List<MovablePoint> sarasas) {
		for ( MovablePoint g:sarasas) {
			g.moveLeft();
		}
	}
	
	public static void main(String[] args) {
		MovablePoint taskas = new MovablePoint(2, 4, 2, 2);
		MovableCircle apskritimas = new MovableCircle(2, 6, 4, 6);
		MovableRectangle staciakampis = new MovableRectangle(4, 8, 4, 6, 4, 8);
		
		List<MovablePoint> sarasas = new ArrayList<>();
		
		sarasas.add(taskas);
		sarasas.add(apskritimas);
		sarasas.add(staciakampis);
		
		apskritimas.resizible(50);
		staciakampis.resizible(20);
		

	}

}
