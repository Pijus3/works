package namuDarbai2;

public class MovablePoint implements Movable {
	
	protected int x;
	protected int y;
	protected int SpeedX;
	protected int SpeedY;
	
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public int getSpeedX() {
		return SpeedX;
	}
	public void setSpeedX(int speedX) {
		SpeedX = speedX;
	}
	public int getSpeedY() {
		return SpeedY;
	}
	public void setSpeedY(int speedY) {
		SpeedY = speedY;
	}
	
	public MovablePoint( int x, int y, int SpeedX, int SpeedY ) {
		super();
		this.x = x;
		this.y = y;
		this.SpeedX = SpeedX;
		this.SpeedY = SpeedY;
	}
	
	public String toString() {
		return this.x +" "+this.y+" "+this.SpeedX+" "+this.SpeedY;
	}
	
	public void moveUp() {
		this.y = this.y + this.SpeedY;
	}
	public void moveDown() {
		this.y = this.y - this.SpeedY;
	}
	public void moveRight() {
		this.x = this.x + this.SpeedX;
	}
	public void moveLeft() {
		this.x = this.x - this.SpeedX;
	}
	
}
