
public class paskaita {
	
	public static void informacija( gyvunas g) {
		System.out.println("Gyvunas: "+ g.getName());
		g.garsas();
	}
	public static void skersti ( skerdykla g ) {
		System.out.println("Gauti mesos: " + g.getMesosKiekis());
	}
	public static void vieta( vietinis v ) {
		System.out.println("Gyvuno vardas: "+v.getName());
		System.out.println("Gyvenamos vietos uzima : "+v.getVieta());
	}
	public static void main(String[] args) {
		
		kate murka = new kate();
		sou brisiu = new sou();
		
		murka.garsas();
		brisiu.garsas();
		
		gyvunas[] gyvunai = new gyvunas[3];
		
		gyvunai[0] = new kate();
		gyvunai[1] = new sou();
		gyvunai[2] = new kiaule();
		
		for ( gyvunas g:gyvunai ) {
			g.setName("gyvunas");
			g.garsas();
		}
		sou beausis = new sou();
		
		informacija(beausis);
		informacija(gyvunai[0]);
		informacija(gyvunai[1]);
		
		skersti(new kiaule());
		skersti(new Jautis());
		
		vieta(murka);
		
		
	}

}
