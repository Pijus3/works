package namuDarbai1;

public class Circle extends Shape {
	
	private double radius;
	
	public double getRadius() {
		return radius;
	}
	public void setRadius(double radius) {
		this.radius = radius;
	}
	public double getArea() {
		double S = Math.PI * Math.pow(this.radius, 2); 
		return S;
	}
	public double getPerimeter() {
		double P = 2 * Math.PI * this.radius;
		return P;
	}
	
	

}
