package paskaita;

import java.awt.event.ActionEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class vykdymas {

	public static void main(String[] args) {
		
		Gija g = new Gija();
		g.start();
		
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel panele = new JPanel();
		JButton bt = new JButton("Paleisti");
		JButton stabdyti = new JButton("Sustabdyti");
		
		bt.addActionListener((ActionEvent e) -> {
			g.started=true;
			g.skaicius = 100;
		});
		
		stabdyti.addActionListener((ActionEvent e) -> {
			g.started=false;
		});
			
		
		panele.add(bt);
		panele.add(stabdyti);
		frame.add(panele);
		
		frame.setSize(600, 600);
		frame.setVisible(true);
	}

}
