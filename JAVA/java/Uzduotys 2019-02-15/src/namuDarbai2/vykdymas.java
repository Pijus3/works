package namuDarbai2;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class vykdymas {

	public static void main(String[] args) throws FileNotFoundException {
		
		Scanner in = new Scanner (new File("duomenys.txt"));
		List<duomenys> darbuotojuSarasas = new LinkedList<>();
		duomenys darbuotojas1 = new duomenys() {
			protected int age;
			protected String name;
			public String getName() {
				return this.name;
			}

			public int getAge() {
				return this.age;
			}

			public int setAge(int age) {
				return this.age = age;
			}

			public String setName(String name) {
				return this.name = name;
			}
			public String toString() {
				return this.name + " "+ this.age;
			}
			
		}; 
		darbuotojas1.setName(in.next());
		darbuotojas1.setAge(in.nextInt());
		
		duomenys darbuotojas2 = new duomenys() {
			protected String name;
			protected int age;
			public String getName() {
				return this.name;
			}
			public int getAge() {
				return this.age;
			}
			public int setAge(int age) {
				return this.age = age;
			}
			public String setName(String name) {
				return this.name = name;
			}
			public String toString() {
				return this.name + " "+ this.age;
			}
			
		};
		darbuotojas2.setName(in.next());
		darbuotojas2.setAge(in.nextInt());
		
		duomenys darbuotojas3 = new duomenys() {
			protected String name;
			protected int age;
			public String getName() {
				return this.name;
			}
			public int getAge() {
				return this.age;
			}
			public int setAge(int age) {
				return this.age = age;
			}
			public String setName(String name) {
				return this.name = name;
			}
			public String toString() {
				return this.name + " "+ this.age;
			}
			
		};
		darbuotojas3.setName(in.next());
		darbuotojas3.setAge(in.nextInt());
		
		darbuotojuSarasas.add(darbuotojas1);
		darbuotojuSarasas.add(darbuotojas2);
		darbuotojuSarasas.add(darbuotojas3);
		
		System.out.println(darbuotojuSarasas);
		
		
		in.close();
	}

}
