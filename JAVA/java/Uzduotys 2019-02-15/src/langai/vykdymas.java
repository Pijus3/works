package langai;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class vykdymas {

	public static void main(String[] args) {
		
		JFrame f = new JFrame("Lango pavadinimas");
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JButton mygtukas = new JButton("Paspausk");
		JButton mygtukas2 = new JButton("Paspausk du");
		JPanel panele = new JPanel();
		
		panele.add(mygtukas);
		panele.add(mygtukas2);
		f.add(panele);
		
		f.setSize(200, 200);
		f.setVisible(true);
		
		MygtukoActionListener paspaustas = new MygtukoActionListener();
		
		mygtukas.addActionListener(paspaustas);
		
		mygtukas2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Paspaustas du");
			}
		});
		

	}

}
