package paskaita;

public class Prekes {
	public String name;
	public double price;
	
	public Prekes(String name, double price) {
		this.name= name;
		this.price= price;
	}
	
	public double priceVat() {
		return price*1.21;
	}
	
	
}
