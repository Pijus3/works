package uduotys;
import java.util.Scanner;

public class uzduotis1 {

	public static void main(String[] args) {
		Scanner in = new Scanner (System.in);
		
		System.out.println("Iveskite kiek turime apskiritimu : ");
		int ilgis = in.nextInt();
		int max = 0;
		int y = 0;
		int sk = 0;
		
		int[] masyvas = new int[ilgis];
		
		for (int i=0; i<ilgis; i++) {
			System.out.println("Iveskite apskritimu spindulius : "+ i + " - ");
			masyvas[i] = in.nextInt();
			if (masyvas[i]>max) max=masyvas[i];
		}
		in.close();
		
		while ( y < ilgis ) {
			if (masyvas[y] == max) {
				sk++;
			}
			y++;
		}
		System.out.println("Didziausias skirtulio spindulys : "+ max);
		System.out.println("Didziausias apskritimu yra : "+ sk);
	}

}
