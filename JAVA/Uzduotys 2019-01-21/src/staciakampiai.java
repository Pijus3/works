import java.util.Scanner;

public class staciakampiai {
	public static void main(String[] args) {
		
		var in = new Scanner(System.in);
		System.out.println("Iveskite x1 koordinate : ");
		int x1 = in.nextInt();
		
		System.out.println("Iveskite y1 koordinate : ");
		int y1 = in.nextInt();
		
		System.out.println("Iveskite x2 koordinate : ");
		int x2 = in.nextInt();
		
		System.out.println("Iveskite y2 koordinate : ");
		int y2 = in.nextInt();
		
		in.close();
		
		int Xkrastine = Math.abs(x1-x2);
		int Ykrastine = Math.abs(y1-y2);
			
		int s = Xkrastine * Ykrastine; 
		int p = 2 * (Xkrastine + Ykrastine);
		
		System.out.println("Staciakampio perimetras: " + p);
		System.out.println("Staciakampio plotas: " + s);
	}
}
