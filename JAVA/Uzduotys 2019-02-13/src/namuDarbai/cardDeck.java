package namuDarbai;

import java.util.Collections;
import java.util.LinkedList;

public class cardDeck {
	
	public static LinkedList<Card> deck;
	public static String[] signAr = { "Diamonds", "Hearts", "Clubs", "Spades" };
	public static String[] valueAr = { "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King", "Ace"};
	public static int[] intValueAr = { 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10, 1 };
 	
	public static void generate () {
		deck = new LinkedList<>();
			for ( int i=0; i < signAr.length; i++ ) {
					for ( int z=0; z < intValueAr.length; z++ ) {
						deck.add( new Card(signAr[i], valueAr[z], intValueAr[z]));
					}
			}
	}
	
	public static void mix() {
		Collections.sort(deck);
	}
	
	public static Card drawCard() {
		 return deck.pop();
	}
	
}
