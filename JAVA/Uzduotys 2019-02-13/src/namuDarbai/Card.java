package namuDarbai;

import java.util.Random;

public class Card implements Comparable<Card> {
	
	public String sign;
	public  String value;
	public int intValue;
	
	public Card(String sign, String value, int intValue) {
		super();
		this.sign = sign;
		this.value = value;
		this.intValue = intValue;
	}
	
	public String toString() {
		return sign+" "+value;
	}
 	
	public int compareTo(Card o) {
		Random rand = new Random();
		return rand.nextInt(2)-1;
	}
	
	public int cardValue() {
		int card=0;
	switch (this.value) {
	case "Two":  
		card = 2;
		break;
	case "Three":  
		card = 3;
		break;
	case "Four":  
		card = 4;
		break;
	case "Five":  
		card=5;
		break;
	case "Six":  
		card = 6;
		break;
	case "Seven":  
		card = 7;
		break;
	case "Eight":  
		card = 8;
		break;
	case "Nine":  
		card = 9;
		break;
	case "Ten":  
		card=10;
		break;
	case "Jack":  
		card = 10;
		break;
	case "Queen":  
		card = 10;
		break;
	case "King":  
		card = 10;
		break;
	case "Ace":  
		card = 1;
		break;
	}
	return card;
	}
	
	
}
