package uzduotys;
import java.util.Scanner;
public class uzduotis5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner in = new Scanner (System.in);
		
		System.out.println(" Pirmo apskirtimo spindulys : ");
		double r = in.nextInt();
		System.out.println("  Iveskite skrituliu skaiciuka : ");
		int n = in.nextInt();
		in.close();
		
		int i = 1 ;
		double s = 0;
		
		while ( i <= n ) {
			i *= 2;
			s += Math.PI * Math.pow(r, 2);
			r *= 2;
		}
		System.out.println("Bendras skrituliu plotas : " + s);
			
		
	}

}
