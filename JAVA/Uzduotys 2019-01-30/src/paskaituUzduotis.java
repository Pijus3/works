
public class paskaituUzduotis {
		// 2 uzduotis is klases darbu
		static int[] push ( int[] masyvas, int x) {
			// susikurti laikina masyvas
			int [] tmp = new int[masyvas.length+1];
			//perkopijuoti
			for ( int i=0; i<masyvas.length; i++ ) {
				tmp[i] = masyvas[i];
			}
			//ikelti nauja reiksme
			tmp[tmp.length-1] = x;
			// grazinti 
			return tmp;
		}
		// 3 uzduotis is klases darbu
		static int[] push ( int[] masyvas, int x, int index) {
			// susikurti laikina masyvas vienetu didesni
			int [] tmp = new int[masyvas.length+1];
			//perkopijuoti masyva nuo 0 iki index ir nuo index iki galo 
			for ( int i=0; i<index; i++ ) {
				tmp[i] = masyvas[i];					
				}
			for ( int i=index; i<masyvas.length; i++ ) {
				tmp[i+1] = masyvas[i];					
				}	
			//ikelti nauja reiksme
			tmp[index] = x;
			// grazinti 
			return tmp;
		}
	
	public static void main(String[] args) {
		int[] masyvas = new int[0];
		masyvas = push(masyvas, 5);
		masyvas = push(masyvas, 4);
		masyvas = push(masyvas, 9);
		
		masyvas = push(masyvas, 9, 1);
		
		for ( int i:masyvas ) {
			System.out.print(i + "\t");
		}
		
	}

}
