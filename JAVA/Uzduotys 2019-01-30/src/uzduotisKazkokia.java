
public class uzduotisKazkokia {
	// 4 uzduotis is klases darbu
	static int daugyba ( int a, int b) {
		if ( b== 1 ) return a;
		return a+daugyba(a,b-1);
	}
	// 5 uzduotis is klases darbu
	static int skaiciuKiekis ( int x) {
		if ( x < 10 ) return 1;
		else return 1+skaiciuKiekis(x/10);
	}
	
	public static void main(String[] args) {
		
		System.out.println(daugyba(5,7));
		System.out.println(skaiciuKiekis(1200));

	}

}
