package namuDarbai;

public class Staff {
	
	protected int kelintas = 0;
	protected double suma=0;
	protected StaffMember[] darbuotojai = new StaffMember [6];
	
	public void addStaffMember (StaffMember darbuotojas) {
		darbuotojai[kelintas] = darbuotojas;
		kelintas++;
	}
	
	public double payDay () {
		for ( int i=0; i<darbuotojai.length; i++ ) {
			suma += darbuotojai[i].pay();
			System.out.println(darbuotojai[i] + " sumoketa "+ darbuotojai[i].pay());
		}
		double viskas = suma;
		suma = 0;
		return viskas;
		
	}
	
	
	
}
