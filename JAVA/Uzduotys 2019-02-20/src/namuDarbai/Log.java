package namuDarbai;

public class Log {
	
	private static Log instance=null;
	
	private Log() {
		
	}
	
	private String filename="log.txt";
	private int totalMessage = 0;
	
	public void add(String message, String author) {
		this.totalMessage++;
		
	}
	
	public int getTotalMessage() {
		return this.totalMessage;
	}
	
	public int closeLog() {
		return 1;
	}
	
	public static Log getInstance() {
		if ( instance == null ) {
			instance = new Log();
		}
		return instance;
	}
}
