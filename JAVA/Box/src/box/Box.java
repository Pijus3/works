package box;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Toolkit;
import javax.swing.*;

public class Box extends JPanel {

	private static int RECT_X = 225;
	private static int RECT_Y = 225;
	private static final int RECT_WIDTH = 150;
	private static final int RECT_HEIGHT = 150;
	
	private final int B_WIDTH = 600;
	private final int B_HEIGHT = 600;

	public Box() {
		initMainWindow();
	}

	private void initMainWindow() {

		addKeyListener(new Controls());
		setBackground(Color.black);
		setFocusable(true);

		setPreferredSize(new Dimension(B_WIDTH, B_HEIGHT));

	}

	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		doDrawing(g);
	}

	private void doDrawing(Graphics g) {
		g.setColor(Color.gray);
		g.fillRect(RECT_X, RECT_Y, RECT_WIDTH,RECT_HEIGHT );
		//g.fill3DRect(RECT_X, RECT_Y, RECT_WIDTH, RECT_HEIGHT, true);
		//Toolkit.getDefaultToolkit().sync();
	}
		      
}
