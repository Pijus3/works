package box;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Controls extends KeyAdapter {
	
	protected boolean leftDirection = false;
	protected boolean rightDirection = true;
	protected boolean upDirection = false;
	protected boolean downDirection = false;
	
	public void keyPressed(KeyEvent e) {
		   
		   int key = e.getKeyCode();
		   
		   if ((key == KeyEvent.VK_LEFT) && (!rightDirection)) {
			  leftDirection = true;
			  upDirection = false;
			  downDirection = false;
			  System.out.println("LEFT");
		   }
		   if ((key == KeyEvent.VK_RIGHT) && (!leftDirection)) {
				  rightDirection = true;
				  upDirection = false;
				  downDirection = false;
				  System.out.println("Right");
		   }
		   if ((key == KeyEvent.VK_UP) && (!downDirection)) {
				  upDirection = true;
				  rightDirection = false;
				  leftDirection = false;
				  System.out.println("UP");
		   }
		   if ((key == KeyEvent.VK_DOWN) && (!upDirection)) {
				  downDirection = true;
				  rightDirection = false;
				  leftDirection = false;
				  System.out.println("Down");
		   }
	   }

}
