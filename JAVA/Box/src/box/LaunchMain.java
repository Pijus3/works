package box;

import java.awt.EventQueue;

import javax.swing.JFrame;

public class LaunchMain extends JFrame {
	
	public LaunchMain() {
		initUI();
	}
	private void initUI() {
		add(new Box());

		setResizable(false);
		pack();
		
		setTitle("Box");
		setLocationRelativeTo(null);
	}
	
	public static void main(String[] args) {
		
		//paleidimo vieta
		EventQueue.invokeLater( () -> {
			JFrame ex = new LaunchMain();
			ex.setVisible(true);
		});
	}

}
