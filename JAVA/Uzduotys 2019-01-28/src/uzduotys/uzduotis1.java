package uzduotys;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class uzduotis1 {

	public static void main(String[] args) throws FileNotFoundException {
		Scanner in = new Scanner (new File ("uzduotis1.txt"));
		
		int n = in.nextInt();
		int[] masyvas = new int[n];
		
		for (int i=0; i<n; i++) {
			int atlyginimai = in.nextInt();
			masyvas[i]=atlyginimai;
		}
		for (int i=0; i<masyvas.length; i++) {
			for (int y=0; y<i; y++) {
				if (masyvas[i] < masyvas[y]) {
					int rikiavimas = masyvas[i];
					masyvas[i] = masyvas[y];
					masyvas[y] = rikiavimas;
				}
				
			}
		}
		int sum400=0;
		int sum600=0;
		int sum800=0;
		String daugiausiaiGauna = null;
		
		for (int i=0; i<n; i++) {
			if ( masyvas[i] > 400 && masyvas[i] < 600 ) sum400++;
			if ( masyvas[i] > 600 && masyvas[i] < 800 ) sum600++;
			if ( masyvas[i] > 800 ) sum800++;
		}
		if ( sum400 > sum600 ) {
			daugiausiaiGauna = "Daugiausiai gauna virs 400";
		}
		if( sum400 < sum600 && sum600 > sum800 ) {
			daugiausiaiGauna = "Daugiausiai gauna virs 600";
		}
		if ( sum800 > sum600 && sum800 > sum600 ) {
			daugiausiaiGauna = "Daugiausiai gauna virs 800";
		}
		System.out.println("Mokytoju gaunanciu didesni atlygi nei 400 euru : " +sum400);
		System.out.println("Mokytoju gaunanciu didesni atlygi nei 600 euru : " +sum600);
		System.out.println("Mokytoju gaunanciu didesni atlygi nei 800 euru : " +sum800);
		System.out.println(daugiausiaiGauna);
		
		for (int i=0; i<n; i++) {
			System.out.print(masyvas[i]+ " ");
		}
		in.close();
	}
}