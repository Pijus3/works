package Stack;

public class Stack {
	public Item p = null;
	
	public void push (int i ) {
		if ( p ==null) {
			p=new Item(i);
		}
		else {
			Item tmp = new Item(i);
			tmp.next = p;
			p=tmp;
		}
	}
	
	public void add(int index, int y) {
		Item tmp = p;
		for (int i=0; i<index-1; i++) {
			tmp=tmp.next;
		}
		Item q = new Item(y);
		q.next = tmp.next;
		tmp.next=q;
	}
	
	public void remove(int index) {
		Item tmp = p; 
		if (index ==0 ) {
			p=p.next;
			return;
		}
		for(int y=0; y<index-1; y++) {
			tmp=tmp.next;
		}
		if(tmp.next == null ) {
			tmp.next = null;
		} else {
			tmp.next=tmp.next.next;
		}
	}
	
}
