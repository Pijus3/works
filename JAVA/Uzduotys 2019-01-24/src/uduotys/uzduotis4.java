package uduotys;
import java.util.Scanner;

public class uzduotis4 {

	public static void main(String[] args) {
		Scanner in = new Scanner (System.in);
		
		System.out.println("Iveskite namu skaiciu : ");
		int namai = in.nextInt();
		int suma = 0;
		int sumaDes = 0;
		int sumaKai = 0;
		
		int[] gyventojai = new int[namai];
		
		for ( int i = 0; i < namai; i++) {
			System.out.println("Iveskite gyventoju skaiciu name "+i+" : ");
			gyventojai[i] = in.nextInt();
		}
		
		for (int i=0; i<namai; i++) {
			suma += gyventojai[i];
			if (i % 2 == 0) sumaDes++;
			if (i % 2 != 0) sumaKai++;
		}
		
		int[] kaire = new int[namai];
		int[] desine = new int[namai];

		for (int i=0; i<namai; i++) {
			if (i % 2 != 0) desine[i] = gyventojai[i];
			if (i % 2 == 0) kaire[i] = gyventojai[i];

		}
		
		System.out.println("gyventoju gyvena : " + suma);
		
		int i = 0;
		int gyventojaiDesineje = 0 ; 
		int gyventojaiKaireje = 0;
		
		while ( i < namai) {
		System.out.println("kaire : " + kaire[i]);
		System.out.println("desine : " +  desine[i]);
		gyventojaiDesineje += desine[i];
		gyventojaiKaireje += kaire[i];
		i++;
		}
		in.close();
		System.out.println("gyventoju gyvena Desineje puseje : " + gyventojaiDesineje);
		System.out.println("gyventoju gyvena Kaireje puseje : " + gyventojaiKaireje);
		
		System.out.println("gyventoju vidurkis Desineje puseje : " + (gyventojaiDesineje/sumaDes));
		System.out.println("gyventoju vidurkis Kaireje puseje : " + (gyventojaiKaireje/sumaKai));

	}

}
