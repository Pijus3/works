package uduotys;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class uzduotis5 {

	public static void main(String[] args) throws FileNotFoundException {
		Scanner in = new Scanner (new File ("skaiciai.txt"));
		
		int z = 0;
		int suma = 0;
		// nuskaito kiek poziciju bus skirta masyvui
		while ( in.hasNextInt() ) {
			int i = in.nextInt();
			z++;
		}
		in.close();
		in = new Scanner (new File ("skaiciai.txt"));
		
		int[] skaiciai = new int[z];
		// uzpildo masyva
		for ( int i =0; z>i; i++) {
			skaiciai[i] = in.nextInt();
		}
		// paduoda skaiciu i i sekanti cikla
		for (int i=0; z>i; i++) {
			suma=0;
			// ciklas tikrina kiek kartu pasidalina skaicius nuo 1 iki savo skaitines reiksmes
			for ( int y = 1; y < skaiciai[i]; y++)
			if ( skaiciai[i] % y == 0 ) {
				suma++;
			}
			// jei suma virsija 2 tada sudetinis, jei ne tai pirminis 
			if (suma >= 2 ) System.out.println("Sudetinis : " + skaiciai[i]);
			if (suma < 2 && skaiciai[i] != 1 ) System.out.println("Pirminis : " + skaiciai[i]);
		}
		in.close();
	}

}
