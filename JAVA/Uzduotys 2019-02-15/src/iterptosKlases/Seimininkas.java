package iterptosKlases;

public class Seimininkas {
	
	public String name;
	
	public class Gyvunas {
		
		public String name;
		
		public void getInfo() {
			System.out.println("Vardas: " + name);
			System.out.println("Seimininko vardas: " + Seimininkas.this.name);
		}
	}
	
	public Seimininkas(String name) {
		this.name = name;
	}
	
	public Gyvunas getNewGyvunas() {
		return new Gyvunas();
	}

}
