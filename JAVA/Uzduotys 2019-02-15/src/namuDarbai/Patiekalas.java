package namuDarbai;

import java.util.LinkedList;

public class Patiekalas {
	
	public String name;
	public LinkedList<Produktas> produktai = new LinkedList<>();
	private int totalFat=0;
	private int totalProtein=0;
	private int totalCarbs=0;
	
	public String toString() {
		return this.name;
	}
	
	   class Produktas {
		public  String name;
		public  int fat;
		public  int protein;
		public  int carbs;
		public  int size;
		
		public String toString() {
			return name;
		}
	}
	
	public void addProduct (String name, int fat, int protein, int carbs, int size) {
		Produktas p = new Produktas();
		p.name = name;
		p.fat = fat;
		p.protein = protein;
		p.carbs = carbs;
		p.size = size;
		produktai.add(p);
		this.totalFat += p.fat;
		this.totalProtein += p.protein;
		this.totalCarbs += p.carbs;
	}
	
	public void produktai() {
		Patiekalas.this.produktai();
	}
	
	public int totalFat() {
		return this.totalFat;
		}
	public int totalProtein() { 
		return this.totalProtein;
	}
	public int totalCarbs() {
		return this.totalCarbs;
	}

}
