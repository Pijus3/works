package namuDarbai;

import java.awt.GridLayout;
import java.util.*;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class vykdymas {

	public static void main(String[] args) {
		
		List<Patiekalas> receptuKnyga = new LinkedList<>();

		Patiekalas patiekalas1 = new Patiekalas();
		patiekalas1.name = "Blynai";
		patiekalas1.addProduct("Miltai", 2, 2, 10, 100);
		patiekalas1.addProduct("kiausiniai", 4, 2, 10, 100);
		patiekalas1.addProduct("pienas", 4, 3, 10, 100);
		patiekalas1.addProduct("sokoladas", 5, 2, 10, 100);
		
		Patiekalas patiekalas2 = new Patiekalas();
		patiekalas2.name = "Varskeciai";
		patiekalas2.addProduct("Miltai", 2, 4, 8, 100);
		patiekalas2.addProduct("kiausiniai", 3, 8, 10, 100);
		patiekalas2.addProduct("pienas", 2, 5, 4, 100);
		patiekalas2.addProduct("sokoladas", 7, 9, 5, 100);

		receptuKnyga.add(patiekalas1);	
		receptuKnyga.add(patiekalas2);	
		
		JFrame frame = new JFrame("Receptu knyga");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout (new GridLayout(1, 1));

		Object[] antrastes = { "Pavadinimas", "Riebalai", "Baltymai", "Angliavandeniai"};
		Object[][] duomenys = new Object[4][4];
		
		duomenys[0][0] = patiekalas1.name;
		duomenys[0][1] = patiekalas1.totalFat();
		duomenys[0][2] = patiekalas1.totalProtein();
		duomenys[0][3] = patiekalas1.totalCarbs();
		
		duomenys[1][0] = patiekalas2.name;
		duomenys[1][1] = patiekalas2.totalFat();
		duomenys[1][2] = patiekalas2.totalProtein();
		duomenys[1][3] = patiekalas2.totalCarbs();


		JTable lentele=new JTable(duomenys, antrastes);
		JScrollPane panele=new JScrollPane(lentele);
		
		frame.add(panele);
		lentele.setFillsViewportHeight(true);
		frame.setSize(600, 600);
		frame.setVisible(true);
	}

}
