package paskaitaKitas;

public class Darbuotojas {
	private String name;
	private String position;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public Darbuotojas(String name, String position) {
		super();
		this.name = name;
		this.position = position;
	}
	
	public String toString () {
		return this.name+ " "+this.position;
	}
	
	
	
}
