package namuDarbaiGrafikas;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class vykdymas {

	public static void main(String[] args) {
		
		JFrame f = new JFrame ("Langas");
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.getContentPane().setBackground(Color.BLACK);
		JPanel panele = new JPanel();
		f.add(panele);
		
		JButton bt1=new JButton("y=1/x");
		JButton bt2=new JButton("y=|x|");
		JButton bt3=new JButton("y=x^2");
		JButton bt4=new JButton("y=x^3");
		JButton bt5=new JButton("y=sin(x)");
		JButton bt6=new JButton("y=cos(x)*x");
		
		panele.add(bt1);
		panele.add(bt2);
		panele.add(bt3);
		panele.add(bt4);
		panele.add(bt5);
		panele.add(bt6);
		
		bt1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				drawGraphics panele =new isVieno();
				f.add(panele);
				f.setSize(600,600);
				f.setVisible(true);		
			}
		});
		
		bt2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				drawGraphics panele=new Modulis();
				f.add(panele);
				f.setSize(600,600);
				f.setVisible(true);		
			}
		});
		
		bt3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				drawGraphics panele=new kvadratu();
				f.add(panele);
				f.setSize(600,600);
				f.setVisible(true);		
			}
		});
		
		bt4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				drawGraphics panele=new kubu();
				f.add(panele);
				f.setSize(600,600);
				f.setVisible(true);		
			}
		});
		
		bt5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				drawGraphics panele=new sinusas();
				f.add(panele);
				f.setSize(600,600);
				f.setVisible(true);		
			}
		});
		
		bt6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				drawGraphics panele=new kosinusas();
				f.add(panele);
				f.setSize(600,600);
				f.setVisible(true);		
			}
		});
		
		f.setSize(600,600);
		f.setVisible(true);
		
		

	}

}
