package namuDarbai1;

public class vykdymas {
	
	public static double getPaintCost ( Shape s, double cost ) {
		return s.getArea() * cost;
	}
	public static void main(String[] args) {
		
		Circle apskritimas = new Circle();
		apskritimas.setRadius(2);
		
		Square kvadratas = new Square();
		kvadratas.setSide(4);
		
		System.out.println("Perimetras kvadrato: "+kvadratas.getPerimeter());
		System.out.println("Plotas kvadrato: "+kvadratas.getArea());
		System.out.println("Perimetras apskritimas: "+apskritimas.getPerimeter());
		System.out.println("Plotas apskritimas: "+apskritimas.getArea());

		System.out.println("Moketi uz kvadrato kaina: " +getPaintCost(apskritimas, 3.2));
		System.out.println("Moketi uz kvadrato kaina: " +getPaintCost(kvadratas, 4.3));

	}

}
