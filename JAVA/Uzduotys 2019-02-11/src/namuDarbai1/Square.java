package namuDarbai1;

public class Square extends Shape {
	private double side;
	
	public double getSide() {
		return side;
	}
	public void setSide(int side) {
		this.side = side;
	}
	public double getArea() {
		double S = Math.pow(side, 2);
		return S;
	}
	public double getPerimeter() {
		double P = 4*side;
		return P;
	}

	
	
}
