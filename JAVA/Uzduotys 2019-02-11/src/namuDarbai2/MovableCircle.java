package namuDarbai2;

public class MovableCircle extends MovablePoint implements Movable, Resizible {
	
	protected int radius;
	protected MovablePoint center;
	
	public MovableCircle(int x, int y, int SpeedX, int SpeedY) {
		super(x, y, SpeedX, SpeedY);
	}
	
	public String toString() {
		return this.x+" "+this.y+" "+this.SpeedX+" "+this.SpeedY;
	}
	
	public void moveUp() {
		this.y = this.y + this.SpeedY;
	}
	public void moveDown() {
		this.y = this.y - this.SpeedY;
	}
	public void moveRigth() {
		this.x = this.x + this.SpeedX;
	}
	public void moveLeft() {
		this.x = this.x - this.SpeedX;
	}
	public int resizible( int prec ) {
		return this.radius + (this.radius * prec)/100;
	}
	
}
