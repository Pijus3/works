package trupmenos;

public class trupmenos {

	private int sveikojiDalis;
	private int skaitiklis;
	private int vardiklis;
	// konstruktorius
	public trupmenos(int sveikojiDalis, int skaitiklis, int vardiklis) {
		this.sveikojiDalis = sveikojiDalis;
		this.skaitiklis = skaitiklis;
		this.vardiklis = vardiklis;
	}
	
	public trupmenos(int skaitiklis, int vardiklis) {
		this.skaitiklis = skaitiklis;
		this.vardiklis = vardiklis;
	}
	
	// get set
	public int getSveikojiDalis() {
		return sveikojiDalis;
	}
	public void setSveikojiDalis(int sveikojiDalis) {
		this.sveikojiDalis = sveikojiDalis;
		this.suprastink();
	}
	public int getSkaitiklis() {
		return skaitiklis;
	}
	public void setSkaitiklis(int skaitiklis) {
		this.skaitiklis = skaitiklis;
		this.suprastink();
	}
	public int getVardiklis() {
		return vardiklis;
	}
	public void setVardiklis(int vardiklis) {
		this.vardiklis = vardiklis;
		this.suprastink();
	}
	// metodai
	public String toString() {
		if (this.sveikojiDalis == 0) {
			return getSkaitiklis() + "/" + getVardiklis() ;
		}
		else {
			return getSveikojiDalis() + " " + getSkaitiklis() + "/" + getVardiklis() ;
		}
	}
	
	public void add (int i) {
		this.sveikojiDalis += i;
		this.suprastink();
	}
	
	private void suprastink () {
		int i=1;
		int x=0;
		int z=0;
		while ( i < this.skaitiklis ) {
			i++;
			x=0;
			if ( this.vardiklis % i == 0 && this.skaitiklis % i ==0 ) {
				while ( x < this.vardiklis ) {
					x++;
					z = i;
				}
			}
		}
		if ( z != 0) {
		this.vardiklis = this.vardiklis / z;
		this.skaitiklis = this.skaitiklis / z;
		if ( this.skaitiklis > this.vardiklis ) {
			this.sveikojiDalis = this.skaitiklis / this.vardiklis;
			this.skaitiklis = this.skaitiklis - this.vardiklis;
		} else {
			System.out.println();
		}
		}
	}
	
	public void add (int i, int j) {
		int bendrasVardiklis = vardiklis * j;
		this.skaitiklis = skaitiklis * j + i * vardiklis;
		this.vardiklis = bendrasVardiklis;
		this.suprastink();
	}
	
	public void add ( trupmenos t2 ) {
		this.add(t2.getSveikojiDalis());
		this.add(t2.getSkaitiklis(), t2.getVardiklis());
		this.suprastink();
	}
	/**
	 * Metodas pavercia trupmena i skaiciu
	 * @return double skaicius
	 */
	public double toDouble () {
		return this.sveikojiDalis + (double) this.getSkaitiklis() / this.vardiklis;
	}
	
	// minusas
	
	public int atimtiSveika ( int sveikasisSkaicius ) {
		return this.sveikojiDalis = this.sveikojiDalis - sveikasisSkaicius ;
	}
	
	public void minus ( int i, int j) {
		int bendrasVardiklis = vardiklis * j;
		this.skaitiklis = skaitiklis * j - i * vardiklis;
		this.vardiklis = bendrasVardiklis;
		this.suprastink();
	}
	
	public void minusAll ( int i, int j, int k) {
		this.atimtiSveika(i);
		this.minus(j, k);
		this.suprastink();
	}
	
	public void atimtiTrupmena ( trupmenos t2 ) {
		this.minusAll(t2.getSveikojiDalis(), t2.getSkaitiklis(), t2.getVardiklis());
		this.suprastink();
	}
	
	// daugyba
	
	public int daugyba(int sveikasisSkaicius ) {
		return this.sveikojiDalis = this.sveikojiDalis * sveikasisSkaicius ;
	}
	
	public void padaugintiSkaitiklisVardiklis ( int i, int j ) {
		this.skaitiklis = this.skaitiklis * i;
		this.vardiklis = this.vardiklis * j;
		this.suprastink();
	}
	
	public void padaugintiAll ( int i, int j, int k ) {
		this.sveikojiDalis = this.sveikojiDalis * i;
		this.skaitiklis = this.skaitiklis * j;
		this.vardiklis = this.vardiklis * k;
		this.suprastink();
	}
	
	public void padaugintiTrupmena ( trupmenos t2) {
		this.sveikojiDalis = this.sveikojiDalis * t2.getSveikojiDalis();
		this.skaitiklis = this.skaitiklis * t2.getSkaitiklis();
		this.vardiklis = this.vardiklis * t2.getVardiklis();
		this.suprastink();
	}

}
