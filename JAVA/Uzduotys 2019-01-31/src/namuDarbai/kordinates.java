package namuDarbai;

public class kordinates {

	private int x;
	private int y;
	// set get metodai
	/**
	 *  kordinates klases x reiksme
	 * @return
	 */
	public int getX () {
		return this.x;
	}
	public void setX (int x ) {
		this.x = x;
	}
	/**
	 * kordinates klases y reiksme
	 * @return
	 */
	public int getY () {
		return this.y;
	}
	public void setY (int y) {
		this.y = y;
	}
	// konstruktoriai
	public kordinates (int x) {
		this.x = x;
	}
	public kordinates (int x, int y) {
		this.x = x;
		this.y = y;
	}
	// metodas 
	/**
	 * suapvalina skaicius po kablelio
	 * @param sk
	 * @param perc
	 * @return rounded int by perc
	 */
	public static double round( double sk, int perc) {
		return Math.round(sk*Math.pow(10, perc))/(Math.pow(10, perc));
	}
	/**
	 * atstumas nuo [0:0] koordinaciu
	 * @return distance from origin to your 
	 */
	public double distanceFromOrigin() {
		return round(Math.sqrt((Math.pow(this.x, 2)+Math.pow(this.y, 2))), 2);
	}
	/**
	 * perstumiamas taskas ivestais duomenimis
	 * @param dx x+dx
	 * @param dy y+dy
	 */
	public void perstumti (int dx, int dy) {
		this.x += dx;
		this.y += dy;
	}
	/**
	 * isveda koordinates formatu [x:y]
	 */
	public String toString () {
		return "["+this.x+":"+this.y+"]";
	}
	/**
	 * atstumas tarp objektu
	 * @param x norimas objektas 
	 * @return width from cordinates
	 */
	public double atstumas (kordinates x) {
		return round(Math.sqrt((Math.pow((x.getX()-this.x), 2)+Math.pow((x.getY()-this.y), 2))), 2);
	}
	/**
	 * pakeicia esamo tasko koordinates i naujai ivestas
	 * @param nx x koordinate
	 * @param ny y koordinate
	 */
	public void setLocation (int nx, int ny) {
		this.x = nx;
		this.y = ny;
	}
	
}
