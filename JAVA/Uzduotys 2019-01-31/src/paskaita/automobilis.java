package paskaita;

public class automobilis {

	private String numeris;
	private String marke;
	private String modelis;
	private double variklioTuris;
	// konstruktorius
	public automobilis(String numeris) {
		this.numeris = numeris;
	}
	
	public automobilis(String numeris, String marke) {
		this.numeris = numeris;
		this.marke = marke;
	}
	
	public automobilis(String numeris, String marke, String modelis, double variklioTuris) {
		this.numeris = numeris;
		this.marke = marke;
		this.modelis = modelis;
		this.variklioTuris = variklioTuris;
	}
	// metodas
	public double kuro100 () {
		return this.variklioTuris*3;
	}
	// set ir get 
	public String getNumeris() {
		return this.numeris;
	}
	
	public String getMarke() {
		return this.marke;
	}
	
	public void setMarke(String marke)  {
		this.marke = marke;
	}

	public String getModelis() {
		return modelis;
	}

	public void setModelis(String modelis) {
		this.modelis = modelis;
	}

	public double getVariklioTuris() {
		return variklioTuris;
	}

	public void setVariklioTuris(double variklioTuris) {
		this.variklioTuris = variklioTuris;
	}

	public void setNumeris(String numeris) {
		this.numeris = numeris;
	}
	// metodas
	public String toString () {
		return "Automobilis : "+this.numeris + " "+ this.marke +" "+ this.modelis +" "+ this.variklioTuris;
	}
	
}
