import java.util.Scanner;

public class uzduotis5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner in = new Scanner(System.in);
		
		System.out.println("Kvadratine lygtis atrodo taip ax^2 + bx + c = 0");
		System.out.println("Iveskite a : ");
		int a = in.nextInt();
		System.out.println("Iveskite b : ");
		int b = in.nextInt();
		System.out.println("Iveskite c : ");
		int c = in.nextInt();
		in.close();
		
		if ( a != 0 && b != 0 && c != 0) {
			
			double D = Math.pow(b, 2) - 4*a*c;
			
			if (D == 0) {
				
				double x = -b / 2*a;
				System.out.print("x reiksme : " + x);
			}
			if ( D > 0 ) {
				
				double x1 = (-b-Math.sqrt(D)) / 2*a;
				double x2 = (-b+Math.sqrt(D)) / 2*a;
				System.out.print(" x1 reiksme : " + x1);
				System.out.print(" x2 reiksme : " + x2);
			}
			if ( D < 0 ) {
				System.out.println("Lygtis neturi sprendiniu realiuju skaiciu aibeje");
			}
			
		} else {
			System.out.println("Si lygtis neturi sprendiniu");
		}

	}

}
