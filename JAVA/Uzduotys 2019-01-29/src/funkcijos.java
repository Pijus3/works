
public class funkcijos {
	
	static int suma(int x, int y) {
		return x+y;
	}
	
	static void printKaina (double kaina) {
		System.out.println(kaina + " Eur");
	}
	
	static void printKainaPVM (double kaina) {
		System.out.println(round(kaina*1.21, 2) + " Eur + PVM");
	}
	static double round(double sk, int prec) {
		return Math.round(sk*Math.pow(10, prec))/Math.pow(10, prec);
	}
	static void laikinas (int[][] masyvas ) {
		masyvas[0] = new int[1];
		masyvas[0][0]=5;
	}

	public static void main(String[] args) {
		
		int sk = suma(5,3);
		
		double kainosObouliu = 1;
		double kainosKriausiu = 4.74;
		
		System.out.println(sk);
		
		printKaina(kainosObouliu);
		printKaina(kainosKriausiu);
		
		printKainaPVM(kainosObouliu);
		printKainaPVM(kainosKriausiu);
		
		double sk2 = 1.0 / 3;
		System.out.println( round(sk2, 3));
		
		int[][] sk3 = new int[1][1];
		
		sk3[0][0] = 10;
		laikinas(sk3);
		
		System.out.println(sk3[0][0]);
		
	}

}
