package namuDarbaiDvimaciaiMasyvai;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class uzduotis2 {

	public static void main(String[] args) throws FileNotFoundException {
		Scanner in = new Scanner (new File ("uzduotis2.txt"));
		
		int n = in.nextInt();
		
		int[][] istrizaines = new int [n][n];
		
		for ( int i=0; i<istrizaines.length; i++) {
			for ( int y=0; y<istrizaines[i].length; y++) {
				istrizaines[i][y] = in.nextInt();
			}
		}
		
		for ( int i=0; i<istrizaines.length; i++ ) {
			istrizaines[i][i]=0;
			istrizaines[i][istrizaines.length-i-1]=0;
		}
		
		
		for ( int i=0; i<istrizaines.length; i++) {
			for ( int y=0; y<istrizaines[i].length; y++) {
				System.out.print(istrizaines[i][y] + "\t");
			}
			System.out.println();
		}
		
		in.close();
	}

}
