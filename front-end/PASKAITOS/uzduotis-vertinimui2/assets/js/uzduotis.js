var CR =document.getElementById('charactersRemaining');
var sendForm = document.getElementById('sendForm');
var form = document.forms['myForm']; 
var msgError    = document.getElementById('msgError'); //klaidos pranešimo DIV
var msgSuccess  = document.getElementById('msgSuccess'); //sėkmingo pranešimo DIV

// Message laukelio tikrinimas ar nevirsytas leistinu simboliu kiekis

document.getElementById('message').addEventListener('keyup', countCharacters, false);                                                                    

function countCharacters(e) { 
    e.preventDefault();                                   
    var textEntered, countRemaining, counter;          
        textEntered = document.getElementById('message').value;  
        counter = (250 - (textEntered.length));
        countRemaining = CR; 
        countRemaining.textContent = counter; 
  if (counter <= 0 ) {
        console.log("veikia");
        CR.innerHTML= "No more text symbols allowed !";
        form.message.style.border="1px solid red";
        form.message.maxLength="250";
  } else {
    form.message.style.border="none";
  }
}
// Formos tikrinimas ar visi laukeliai uzpildyti 
form.addEventListener('submit', validation); 

function validation (event) {
    event.preventDefault();
    let error = false;
     
    if (form['name'].value == ''){
        form['name'].style.border="1px solid red";
        showError("Enter your name");
        error = true;
    }
    if (form['email'].value == ''){
        form['email'].style.border="1px solid red";
        showError('Enter your mail')
        error = true;
    }
    if (form['title'].value == ''){
        form['title'].style.border="1px solid red";
        showError('Enter title')
        error = true;
    }
    if (form['message'].value == ''){
        form['message'].style.border="1px solid red";
        showError('Enter your message')
        error = true;
    }
    if (form['name'].value == '' && form['email'].value == '' && form['title'].value == '' && form['message'].value == '') {
        form['title'].style.border="1px solid red";
        form['email'].style.border="1px solid red";
        form['message'].style.border="1px solid red";
        form['name'].style.border="1px solid red";
        showError('Please fill the box');
        error = true;
    }
    if (error != true) {
        msgError.style.display   = "none";
        msgSuccess.style.display = "block";
        msgSuccess.innerText     = "Žinutė išsiųsta";
    } 
}
function showError(error) {
    msgError.innerText = "Įvyko klaida: " + error;
    msgError.style.display = "block";
}

// Features laukeliu uzsikrovimas
var features = {projects: 584, hours: 1465, feedbacks: 612, clients: 735};


function cycle() {
    let num=0;

    while  (num<features.projects) {
        num++;
        title.innerHTML=num;
    }
}
cycle();